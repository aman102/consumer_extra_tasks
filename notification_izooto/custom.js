// addEventTrackingObj("redirect", "notifyme", "success", false);

// redirectToNearbuy(false, false);

let redirectionUrlMapper = {
  chennai:
    "https://www.nearbuy.com/offers/chennai/collection/top-rated?utm_source=elp&utm_medium=referral&utm_campaign=notification_capture",
  hydrabad:
    "https://www.nearbuy.com/offers/hyderabad/collection/top-rated?utm_source=elp&utm_medium=referral&utm_campaign=notification_capture"
};

window._izq.push([
  "register_callback",
  function(obj) {
    if (obj.statuscode == 1) {
        addEventTrackingObj("click", "permission", "allow", true);
        toggleToast("allowedNotificationToast");
    } else if (obj.statuscode == 3) {
       addEventTrackingObj("click", "permission", "block", true);
       toggleToast("blockedNotificationToast");
      
    }
  }
]);

function getPermissions() {
  window._izq.push(["triggerPrompt"]);
  addEventTrackingObj("click", "permission", "initiate", true);
}

function toggleToast(id) {
  document.getElementById(id).classList.add("toast--show");
  setTimeout(function() {
    document.getElementById(id).classList.remove("toast--show");
    redirectToNearbuy(false);
  }, 3000);
}

// Data Layer Functions
function addEventTrackingObj(eventName, category, action, interaction) {
  let eventObj = {
    event: eventName,
    eventCategory: category,
    interaction: interaction,
  };
  if (action) {
    eventObj.eventAction = action;
  }
  addToDatalayer(eventObj);
}

function addToDatalayer(arg) {
  if ("requestIdleCallback" in window) {
    requestIdleCallback(() => {
      window["dataLayer"].push(arg);
    });
  } else {
    window["dataLayer"].push(arg);
  }
}

function redirectToNearbuy(fireEventInDl) {
  let href = redirectionUrlMapper[city];
  if (fireEventInDl) {
    addEventTrackingObj("click", "mweb", "", true);
  }
  setTimeout(function() {
    window.location.href = href;
  }, 50);
}

