let startApp = function() {
  gapi.load("auth2", function() {
    auth2 = gapi.auth2.init({
      client_id:
        "673187025070-7gjqiv8a1f0q29msrh2ff3hecvm4d866.apps.googleusercontent.com"
    });
    attachSignin(document.getElementById("customSignInBtn"));
  });
};

function attachSignin(element) {
  auth2.attachClickHandler(
    element,
    {},
    function(googleUser) {
      let profile = googleUser.getBasicProfile(),
        googleId = profile.getId(),
        name = profile.getName(),
        emailId = profile.getEmail(),
        timestamp = new Date().getTime();
      addEventTrackingObj("click", "notifyme", "success", false);
      if (googleId && name && emailId) {
        let data = {
          googleId: googleId,
          name: name,
          emailId: emailId,
          timestamp: timestamp
        };
        element.disabled = true;
        saveDataInDb(data);
      } else {
        element.disabled = false;
      }
    },
    function(error) {
      addEventTrackingObj("click", "notifyme", "fail", false);
      redirectToNearbuy(false, false);
    }
  );
}

function saveDataInDb(data) {
  let url = "https://iknpld0b9a.execute-api.ap-southeast-1.amazonaws.com/New_user_engagement";
  $.ajax({
    method: "POST",
    url: url,
    data: JSON.stringify(data)
  })
    .done(function(success) {
      toggleToast('toaster');
    })
    .fail(function(error) {
      redirectToNearbuy(false, false);
    });
}

function toggleToast(id) {
  document.getElementById(id).classList.add("toast--show");
  setTimeout(function() {
    document.getElementById(id).classList.remove("toast--show");
    redirectToNearbuy(true, false);
  }, 3000);
}

function pushDataInDL() {
  addEventTrackingObj("click", "notifyme", "initiate", true);
}

// Data Layer Functions
function addEventTrackingObj(eventName, category, action, interaction) {
  let eventObj = {
    event: eventName,
    eventCategory: category,
    interaction: interaction,
    cd_timestamp: new Date().getTime(),
    cd_page_type: "elp",
    cd_page_label: "lp1"
  };
  if (action) {
    eventObj.eventAction = action;
  }
  addToDatalayer(eventObj);
}

function addToDatalayer(arg) {
  if ("requestIdleCallback" in window) {
    requestIdleCallback(() => {
      window["dataLayer"].push(arg);
    });
  } else {
    window["dataLayer"].push(arg);
  }
}

// Don't Delete this code
// showSubscribedToast : query params is added to show toast message of subscription over Nearbuy.com
let redirectionUrlMapper = {
  'delhi-ncr' : 'https://www.nearbuy.com/offers/delhi-ncr/collection/top-rated?utm_source=elp&utm_medium=referral&utm_campaign=email_capture',
  'pune' : 'https://www.nearbuy.com/offers/pune/collection/top-rated?utm_source=elp&utm_medium=referral&utm_campaign=email_capture'
};

function redirectToNearbuy(showSubscribedToast, fireEventInDl) {
  let href = redirectionUrlMapper[city];
  if (showSubscribedToast) {
    href = href + "&utm_content=email_verified";
  }
  if (fireEventInDl) {
    addEventTrackingObj("click", "mweb", "", true);
  }
  setTimeout(function() {
    window.location.href = href;
  }, 50);
}

window.addEventListener("load", function() {
  startApp();
});
